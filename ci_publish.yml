#
# CI job template
#

# the following variables need to be overridden in the including job definition
variables:
    APP_DOCKERHUB_NAME: "unknown"
    APP_DESC: "Please set up the variables sections"
    APP_PORT: "9999"

    DOCKER_README: README.md  # default README to publish to docker registry
    DOCKER_SNAPSHOT_IMAGE: "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHA"
    DOCKER_HEALTHCHEK: "true"
    DOCKER_HOST: "tcp://docker:2375"
    DOCKER_TLS_CERTDIR: ""


# jobs
# -------------------------------------------------------------------------------

# docker-lint
# can be manually triggered after a push for liniting the Dockerfile 
docker_lint:
  stage: lint
  image: $CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX/hadolint/hadolint:latest-alpine
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual
    - changes:
      - Dockerfile 
      when: always 
  allow_failure: true 
  variables:
    files_to_lint: ""
  script:
    - mkdir -p reports
    - hadolint -f gitlab_codeclimate Dockerfile > reports/hadolint-$(md5sum Dockerfile | cut -d" " -f1).json
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    expire_in: 1 week
    when: always
    reports:
      codequality:
        - "reports/*"
    paths:
      - "reports/*"

# docker build
# runs on scheduled builds, merge requests and any changes to the Dockerfile or testing subdir
docker_build:
  stage: build
  image: $CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX/docker:latest
  services:
    - "docker:dind"
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: always
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: always
    - changes:
        - Dockerfile
        - testing/**/*
      when: always
  before_script:
    - echo -n $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  script:
    - >
      docker build
      --label "org.opencontainers.image.title=$CI_PROJECT_TITLE"
      --label "org.opencontainers.image.url=$CI_PROJECT_URL"
      --label "org.opencontainers.image.created=$CI_JOB_STARTED_AT"
      --label "org.opencontainers.image.revision=$CI_COMMIT_SHA"
      --label "org.opencontainers.image.version=$CI_COMMIT_REF_NAME"
      --tag $DOCKER_SNAPSHOT_IMAGE
      .
    - docker push $DOCKER_SNAPSHOT_IMAGE
    # if building for main branch, tag and push as :latest
    - |
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        echo "Running on main branch"
        docker tag $DOCKER_SNAPSHOT_IMAGE $CI_REGISTRY_IMAGE:latest 
        docker push $CI_REGISTRY_IMAGE:latest
      fi
    - docker history $DOCKER_SNAPSHOT_IMAGE
    - docker images --digests 
    # create dotenv file
    - image_with_digest=$(docker inspect --format '{{index .RepoDigests 0}}' "$DOCKER_SNAPSHOT_IMAGE")
    - docker_digest=${image_with_digest##*@}
    - docker_repository=${DOCKER_SNAPSHOT_IMAGE%:*}
    - docker_tag=${DOCKER_SNAPSHOT_IMAGE##*:}
    - echo "docker_image=$DOCKER_SNAPSHOT_IMAGE" > docker.env
    - echo "docker_image_digest=$docker_repository@$docker_digest" >> docker.env
    - echo "docker_repository=$docker_repository" >> docker.env
    - echo "docker_tag=$docker_tag" >> docker.env
    - echo "docker_digest=$docker_digest" >> docker.env
  artifacts:
    reports: 
      dotenv: 
        - docker.env 

# start up image and test healthcheck 
# to disable on images not using the DOCKER HEALTHCHECK mechanism, set DOCKER_HEALTHCHEK to false 
# this clones its own repo into a subdir to get access to the wfs script in the repo
docker_test:
  image: $CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX/docker:latest
  services:
    - "docker:dind"
  stage: build
  needs: ['docker_build']
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: always
    - if: '$DOCKER_HEALTHCHEK != "true"'
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
    - if: $CI_PIPELINE_SOURCE == "schedule"
      when: always
    - changes:
        - Dockerfile
        - testing/**/*
      when: always
  before_script:
    - echo -n $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - docker stop $APP_DOCKERHUB_NAME || true
  script:
    - apk add bash git
    - git clone https://gitlab.com/homesrvr/defaults.git
    - docker run -d --rm -p $APP_PORT:$APP_PORT --name $APP_DOCKERHUB_NAME $DOCKER_SNAPSHOT_IMAGE 
    - ./defaults/wfs.sh $APP_DOCKERHUB_NAME 20 10
  after_script:
    - docker stop $APP_DOCKERHUB_NAME 


# Publish image to Docker Registry
# -----------------------------------------
publish_image:
  image: $CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX/docker:20
  services:
    - "docker:dind"
  stage: publish
  only:
    refs:
      - main 
      - schedules
  variables:
    GIT_STRATEGY: none
  before_script:
    # see https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
    - echo -n $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  script:
    - docker pull $CI_REGISTRY_IMAGE:latest 
    - echo "Publishing release '$RELEASE'" 
    - docker image ls $CI_REGISTRY_IMAGE:latest 
    - echo "Tagging release ..."
    - docker tag $CI_REGISTRY_IMAGE:latest $DOCKER_USER/$APP_DOCKERHUB_NAME:$RELEASE
    - docker tag $CI_REGISTRY_IMAGE:latest $DOCKER_USER/$APP_DOCKERHUB_NAME:latest
    - docker image ls
    - docker logout $CI_REGISTRY
    # login to docker hub
    - echo "Logging into Docker Hub ..."
    - echo -n $DOCKER_PASSWORD | docker login -u $DOCKER_USER --password-stdin docker.io
    - docker push -q $DOCKER_USER/$APP_DOCKERHUB_NAME:$RELEASE
    - docker push -q $DOCKER_USER/$APP_DOCKERHUB_NAME:latest
  after_script:
    - docker logout docker.io
    - docker logout $CI_REGISTRY

# Publish README to Docker Registry
# Name of Readme $DOCKER_README can be overridden
# -------------------------------------------------
publish_readme:
  stage: publish
  only:
    refs:
      - main 
      - schedules
  needs: 
    - job: publish_image
  image:
    name: $CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX/chko/docker-pushrm
    entrypoint: ["/bin/sh", "-c", "/docker-pushrm"]
  before_script:
    # see https://docs.gitlab.com/ce/ci/variables/predefined_variables.html
    - echo -n $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
  services: []
  variables:
    DOCKER_USER: $DOCKER_USER
    DOCKER_PASS: $DOCKER_PASSWORD
    PUSHRM_SHORT: $APP_DESC
    PUSHRM_TARGET: docker.io/$DOCKER_USER/$APP_DOCKERHUB_NAME
    PUSHRM_DEBUG: 1
    PUSHRM_FILE: $CI_PROJECT_DIR/$DOCKER_README
  script: "/bin/true"


# Publish badge with release version number as artifact
# note: due to a bug in gitlab runner a workaround is being used to make the RELEASE var available
# -------------------------------------------------
publish_badge:
  stage: publish
  image: $CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX/python:3.6.6
  only:
    refs:
      - main 
      - schedules
  needs: 
    - job: publish_image
    - job: get_release
      artifacts: true
  variables:
    GIT_STRATEGY: none
    RELEASE: $RELEASE 
  script:
    - pip install anybadge
    - echo "Creating Badge '$APP_DOCKERHUB_NAME-release' for release '$RELEASE'"
    - anybadge -l "$APP_DOCKERHUB_NAME-release" -v $RELEASE -f release.svg -c blue
    - anybadge -l "dockerimage" -v "link" -f dockerimage.svg -c black
  artifacts:
    paths:
        - release.svg
        - dockerimage.svg

